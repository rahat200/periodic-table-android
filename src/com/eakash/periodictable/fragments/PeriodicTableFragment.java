package com.eakash.periodictable.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.eakash.periodictable.R;
import com.eakash.periodictable.R.id;
import com.eakash.periodictable.misc.Element;
import com.eakash.periodictable.misc.MiniElementView;
import com.eakash.periodictable.misc.TwoDScrollView;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.CellIdentityCdma;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PeriodicTableFragment extends Fragment implements View.OnClickListener{

	  
	  private final int ELEMENT_COUNT = 118;
	  private final int GROUP_COUNT = 18;
	  private LinearLayout elementBigView;
	  private List<Element> elementList;
	  private int mainMargin = -1;
	  //private int miniMargin = -1;
	  private MiniElementView prevElementView;
	  private TextView prevGroupView;
	  private TextView prevPeriodView;
	  //private SharedPreferences ptSettings = null;
	  private RelativeLayout rlMain = null;
	  
	  public PeriodicTableFragment() {
		  
	  }
	  
	  private LinearLayout createGroupHeaders(int cellsize) {
		  
		  LinearLayout groupLayout = new LinearLayout(getActivity());
		  groupLayout.setOrientation(LinearLayout.HORIZONTAL);
		  
		  LinearLayout.LayoutParams groupLayoutParams = new LinearLayout.LayoutParams
				  (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		  groupLayout.setLayoutParams(groupLayoutParams);
		  //groupLayout.setBackgroundResource(R.drawable.border_period_and_group);
		  
		  LinearLayout.LayoutParams groupItemParams = new LinearLayout.LayoutParams
				  (cellsize,cellsize/2);
		  groupItemParams.setMargins(2, 2, 2, 2);
		  
		  //leftmost extra blank space 
		  LinearLayout.LayoutParams extraSpaceParams = new LinearLayout.LayoutParams
				  (cellsize/2,cellsize/2);
		  extraSpaceParams.setMargins(1, 1, 1, 1);
		  View extraSpaceView = new View(getActivity());
		  extraSpaceView.setLayoutParams(extraSpaceParams);
		  groupLayout.addView(extraSpaceView);
		  
		  for(int i = 1;i<=GROUP_COUNT;i++) {
			  TextView tvGroup = new TextView(getActivity());
			  tvGroup.setTag("tvGroup"+i);
			  tvGroup.setText(String.valueOf(i));
			  tvGroup.setTextSize(8);
			  tvGroup.setTypeface(null,Typeface.BOLD);
			  tvGroup.setGravity(Gravity.CENTER);
			  tvGroup.setLayoutParams(groupItemParams);
			  groupLayout.addView(tvGroup);
		  }
		  
		  return groupLayout;
		  
	  }
	  
	  private TextView createPeriodHeaderView(int cellsize,int period) {
		   LinearLayout.LayoutParams periodTextParams = new LinearLayout.LayoutParams(cellsize/2,cellsize);
		   periodTextParams.setMargins(1, 1, 1, 1);
		   TextView tvPeriod = new TextView(getActivity());
		   tvPeriod.setText(Integer.toString(period));
		   tvPeriod.setTextSize(8);
		   tvPeriod.setTypeface(null,Typeface.BOLD);
		   tvPeriod.setTag("tvPeriod" + period);
		   tvPeriod.setGravity(Gravity.CENTER);
		   tvPeriod.setLayoutParams(periodTextParams);
		   return tvPeriod;
	    
	  }
	  
	  @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mainMargin = getResources().getDimensionPixelSize(R.dimen.main_margin);
		
		elementList = getElementList();
		generatePeriodicTable();
		
	}
	  
	public void generatePeriodicTable() {
		
		int displayWidth = 0;  
		int displayHeight = 0; 
		
		int actionBarHeight = 0;
		
		int cellSize;
		int tableWidth;
		
		//get display width and height
		Point size = new Point();
		WindowManager wm = getActivity().getWindowManager();
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)    {
		    wm.getDefaultDisplay().getSize(size);
		    displayWidth = size.x;
		    displayHeight = size.y; 
		}else{
		    Display d = wm.getDefaultDisplay(); 
		    displayWidth = d.getWidth(); 
		    displayHeight = d.getHeight(); 
		}
		
		//get actionbar height
		TypedValue tval = new TypedValue();
		if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tval, true)) {
		    actionBarHeight = TypedValue.complexToDimensionPixelSize(tval.data,getResources().getDisplayMetrics());
		}
		
		cellSize = (displayHeight - 2*mainMargin-actionBarHeight-36-5)/11;
		tableWidth = (cellSize+mainMargin)*18 + 1*(cellSize/2); 

		
		
	    this.rlMain = ((RelativeLayout)getActivity().findViewById(R.id.rlMainTable));
	    RelativeLayout.LayoutParams mainTableLayoutParams = new RelativeLayout.LayoutParams(tableWidth, -2);
	    mainTableLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
	    
	    LinearLayout mainTableLayout = new LinearLayout(getActivity());
	    mainTableLayout.setLayoutParams(mainTableLayoutParams);
	    mainTableLayout.setOrientation(LinearLayout.VERTICAL);
	    
	    mainTableLayout.addView(createGroupHeaders(cellSize));
	    
	    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams
	    		(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	    
	    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams
	    		(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	    
	    LinearLayout top3PeriodsLayout = new LinearLayout(getActivity());
	    top3PeriodsLayout.setLayoutParams(params1);
	    top3PeriodsLayout.setOrientation(LinearLayout.HORIZONTAL);
	    top3PeriodsLayout.setGravity(Gravity.BOTTOM);
	    
	    mainTableLayout.addView(top3PeriodsLayout);
	    
	    LinearLayout top3PeriodsHeaderLayout = new LinearLayout(getActivity());
	    top3PeriodsHeaderLayout.setLayoutParams(params1);
	    top3PeriodsHeaderLayout.setOrientation(LinearLayout.VERTICAL);
	    
	    top3PeriodsLayout.addView(top3PeriodsHeaderLayout);
	    
	    for (int i = 1; i <= 3; i++) {
	    	top3PeriodsHeaderLayout.addView(createPeriodHeaderView(cellSize, i));
	    }
	    
	    int[] top3PeriodsElements = { 1, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15, 8, 16, 9, 17, 2, 10, 18 };
	    
	    LinearLayout top3PeriodsGroup1 = new LinearLayout(getActivity());
	    top3PeriodsGroup1.setLayoutParams(params1);
	    top3PeriodsGroup1.setOrientation(LinearLayout.VERTICAL);
	    
	    top3PeriodsLayout.addView(top3PeriodsGroup1);
	    
	    int elementCount = 1;
	    
	    for (int i = 1; i <= 3; i++)//3 times
	    {
	      
	      MiniElementView miniElementView = new MiniElementView
	    		  (getActivity(), (Element)this.elementList.get(top3PeriodsElements[elementCount-1]-1),cellSize);
	      miniElementView.setTag(Integer.valueOf(elementCount));
	      miniElementView.setOnClickListener(this);
	      top3PeriodsGroup1.addView(miniElementView);
	      
	      elementCount++;
	    }
	    
	    LinearLayout top3PeriodsGroup2 = new LinearLayout(getActivity());
	    top3PeriodsGroup2.setLayoutParams(params1);
	    top3PeriodsGroup2.setOrientation(LinearLayout.VERTICAL);
	    
	    top3PeriodsLayout.addView(top3PeriodsGroup2);
	    
	    for (int i = 1; i <= 2; i++)//2 times
	    {
	      
	      MiniElementView miniElementView = new MiniElementView
	    		  (getActivity(),(Element)this.elementList.get(top3PeriodsElements[elementCount-1]-1),cellSize);
	      miniElementView.setTag(Integer.valueOf(elementCount));
	      miniElementView.setOnClickListener(this);
	      top3PeriodsGroup2.addView(miniElementView);
	      
	      elementCount++;
	    }
	    
	   
	    //start of big element 
	    
	    LinearLayout middleContentLayout = new LinearLayout(getActivity());
	    
	    LinearLayout.LayoutParams middleContentParams = new LinearLayout.LayoutParams
	    		(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
	    //middleContentParams.setMargins( mainMargin, mainMargin, mainMargin,  mainMargin);
	    middleContentParams.weight = 1.0F;
	    middleContentLayout.setLayoutParams(middleContentParams);
	    middleContentLayout.setOrientation(LinearLayout.HORIZONTAL);
	    
	    top3PeriodsLayout.addView(middleContentLayout);
	    
	    elementBigView = ((LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.element_big, null));
	    
	    
	    /*View.OnClickListener local1 = new View.OnClickListener()
	    {
	      public void onClick(View paramAnonymousView)
	      {
	        PeriodicTableClassicFragment.this.showElementDetail(PeriodicTableClassicFragment.this.prevElementView);
	      }
	    };
	    localLinearLayout7.setOnClickListener(local1); */
	    
	    LinearLayout.LayoutParams bigElementParams = new LinearLayout.LayoutParams
	    		(cellSize*3+12, LinearLayout.LayoutParams.MATCH_PARENT);
	    bigElementParams.setMargins(0, 0,0, 0);
	    elementBigView.setLayoutParams(bigElementParams);
	    middleContentLayout.addView(this.elementBigView);
	    
	    LinearLayout legendLayout = (LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.legend, null);
	    LinearLayout.LayoutParams legendParams = new LinearLayout.LayoutParams
	    		(cellSize*7+25,LinearLayout.LayoutParams.MATCH_PARENT);
	    legendParams.setMargins(mainMargin, 0, 0, 0);
	    legendLayout.setLayoutParams(legendParams);
	    
	    middleContentLayout.addView(legendLayout);
	    
	    //end of Big Element
	   
	    
	    
	   
	    for (int i = 1; i <= 5 ; i++)//5 times
	    {
	      LinearLayout top3PeriodsGroup13to17 = new LinearLayout(getActivity());
	      top3PeriodsGroup13to17.setLayoutParams(params1);
	      top3PeriodsGroup13to17.setOrientation(LinearLayout.VERTICAL);
	    
	      top3PeriodsLayout.addView(top3PeriodsGroup13to17);
	      
	      for (int j = 1; j <= 2; j++) // 2 times
	      {
	        
	        MiniElementView miniElementView = new MiniElementView
	        		(getActivity(),(Element)this.elementList.get(top3PeriodsElements[elementCount-1]-1),cellSize);
	        miniElementView.setTag(Integer.valueOf(elementCount));
	        miniElementView.setOnClickListener(this);
	        top3PeriodsGroup13to17.addView(miniElementView);
	        
	        elementCount++;
	      }
	   
	    }
	    
	    
	    LinearLayout top3PeriodsGroup18 = new LinearLayout(getActivity());
	    top3PeriodsGroup18.setLayoutParams(params1);
	    top3PeriodsGroup18.setOrientation(LinearLayout.VERTICAL);
	    
	    top3PeriodsLayout.addView(top3PeriodsGroup18);
	    
	    for (int i = 1; i <= 3; i++)//3 times
	    {
	      
	      MiniElementView miniElementView = new MiniElementView
	    		  (getActivity(),(Element)this.elementList.get(top3PeriodsElements[elementCount-1]-1),cellSize);
	      miniElementView.setTag(Integer.valueOf(elementCount));
	      miniElementView.setOnClickListener(this);
	      top3PeriodsGroup18.addView(miniElementView);
	      
	      elementCount++;
	    }
	    
	//end of top 3 periods 
	    
	    
	    
	    int row = 4;
	    while(row <= 10) {
	    	if(row == 8) {
	    		//divider between lathanoids and actinoids
	    		LinearLayout.LayoutParams blankViewParams = new LinearLayout.LayoutParams
	    				(LinearLayout.LayoutParams.MATCH_PARENT, 5);
	    		View blankView = new View(getActivity());
	    		blankView.setLayoutParams(blankViewParams);
	    		mainTableLayout.addView(blankView);
	    		row++;
	    	}
	    	else {
	    		LinearLayout eachRowLayout = new LinearLayout(getActivity());
	    		eachRowLayout.setLayoutParams(params1);
	    		eachRowLayout.setOrientation(LinearLayout.HORIZONTAL);
	            mainTableLayout.addView(eachRowLayout);
	            
	            if(row == 9 ) {
	            	//row for lathanoids
	            	
	            	//blank view at left side of lathanoids
	            	LinearLayout.LayoutParams blankViewParams = new LinearLayout.LayoutParams
		    				(cellSize*3,LinearLayout.LayoutParams.MATCH_PARENT);
	            	//blankViewParams.weight = 1.0F;
		    		View blankView = new View(getActivity());
		    		blankView.setLayoutParams(blankViewParams);
		    		eachRowLayout.addView(blankView);
	            	
	            	elementCount = 57;
	            	while(elementCount <= 71) {
	            		MiniElementView miniElementView = new MiniElementView
          		    		  (getActivity(),(Element)this.elementList.get(elementCount-1),cellSize);
	          		    miniElementView.setTag(elementCount);
	          		    miniElementView.setOnClickListener(this);
	          		    eachRowLayout.addView(miniElementView);
	          		    //eachRowLayout.setGravity(Gravity.RIGHT);
	          		    elementCount++;
	            	}
	            	row++;
	            } 
	            else if(row == 10) {
	            	//actinoides
	            	
	            	//blank view at left side of lathanoids
	            	LinearLayout.LayoutParams blankViewParams = new LinearLayout.LayoutParams
		    				(cellSize*3,LinearLayout.LayoutParams.MATCH_PARENT);
	            	//blankViewParams.weight = 1.0F;
		    		View blankView = new View(getActivity());
		    		blankView.setLayoutParams(blankViewParams);
		    		eachRowLayout.addView(blankView);
	            	
	            	elementCount = 89;
	            	while(elementCount <= 103) {
	            		MiniElementView miniElementView = new MiniElementView
          		    		  (getActivity(),(Element)this.elementList.get(elementCount-1),cellSize);
	          		    miniElementView.setTag(elementCount);
	          		    miniElementView.setOnClickListener(this);
	          		    eachRowLayout.addView(miniElementView);
	          		    elementCount++;
	            	}
	            	row++;
	            }
	            else {
	            	
	            	for(int i = 0; i <= 18; i++) {
	            		
	            		if(i == 0) {
	            			eachRowLayout.addView(createPeriodHeaderView(cellSize, row));
	            		}
	            		
	            		else {
	            		   	if(elementCount == 57) {
		            			//show view for element 57
	            		   		MiniElementView miniElementView = new MiniElementView
		            		    		  (getActivity(),(Element)this.elementList.get(elementCount-1),cellSize);
		            		    miniElementView.setTag(elementCount);
		            		    miniElementView.setOnClickListener(this);
		            		    eachRowLayout.addView(miniElementView);
		            		    elementCount = 72;
			            	}
			            	else if(elementCount == 89) {
			            		//show view for element 89
			            		MiniElementView miniElementView = new MiniElementView
		            		    		  (getActivity(),(Element)this.elementList.get(elementCount-1),cellSize);
		            		    miniElementView.setTag(elementCount);
		            		    miniElementView.setOnClickListener(this);
		            		    eachRowLayout.addView(miniElementView);
		            		    elementCount = 104;
			            	}
			            	else {
			            		////show view for other elements
			            		MiniElementView miniElementView = new MiniElementView
		            		    		  (getActivity(),(Element)this.elementList.get(elementCount-1),cellSize);
		            		    miniElementView.setTag(elementCount);
		            		    miniElementView.setOnClickListener(this);
		            		    eachRowLayout.addView(miniElementView);
		            		    elementCount++;
			            	}
	            		
	            		}
	            	}
	            	row++;
	            }
	    	}
	    }
		
		rlMain.addView(mainTableLayout);
	}
	
	@Override
	public void onClick(View clickedview) {
		MiniElementView miniElementView = (MiniElementView) clickedview;
		//change background of selected elements Type
		
		if (prevElementView != null)
	    {
	      prevElementView.setActivated(false);
	      getActivity().findViewById(getLegendViewIdByType(this.prevElementView.getElement().Type)).setBackgroundResource(0);
	    }
		
		miniElementView.setActivated(true);
		getActivity().findViewById(getLegendViewIdByType(miniElementView.getElement().Type))
			.setBackgroundResource(R.drawable.border_type_selected);
		
		//find selected element,group and period
		String elementIdPrefix = "element_" + miniElementView.getElement().AtomicNumber + "_";
	    String groupNo = getString(getResources().getIdentifier(elementIdPrefix + "group", "string", getActivity().getPackageName()));
	    String periodNo = getString(getResources().getIdentifier(elementIdPrefix + "period", "string", getActivity().getPackageName()));
	    
	    //de-highlight the previos view
	    if (prevGroupView != null)
	    {
	      prevGroupView.setBackgroundResource(0);
	      //prevGroupView.setTypeface(Typeface.DEFAULT);
	    }
	    if (prevPeriodView != null)
	    {
	      prevPeriodView.setBackgroundResource(0);
	      //prevPeriodView.setTypeface(Typeface.DEFAULT);
	    }
	    
	    //highlight selected group and period number
	    if (!groupNo.equals(getString(R.string.groupna)))
	    {
	      TextView tvgroup = (TextView)rlMain.findViewWithTag("tvGroup" + groupNo);
	      tvgroup.setBackgroundResource(R.drawable.border_period_and_group);
	      tvgroup.setTypeface(null,Typeface.BOLD);
	      prevGroupView = tvgroup;
	    }
	    TextView tvperiod = (TextView)this.rlMain.findViewWithTag("tvPeriod" + periodNo);
	    tvperiod.setBackgroundResource(R.drawable.border_period_and_group);
	    tvperiod.setTypeface(null,Typeface.BOLD);
	    prevPeriodView = tvperiod;
	    
	    
	    elementBigView.setBackgroundResource(miniElementView.getElement().getTypeDrawableResId(false));
	    
	    ((TextView)elementBigView.findViewById(R.id.tvBigAtomicNumber))
	    .setText(getResources().getIdentifier(elementIdPrefix + "atomic_number"
	    		, "string", getActivity().getPackageName()));
	    
	    ((TextView)elementBigView.findViewById(R.id.tvBigSymbol))
	    .setText(getResources().getIdentifier(elementIdPrefix + "symbol"
	    		, "string", getActivity().getPackageName()));
	    
	    ((TextView)elementBigView.findViewById(R.id.tvBigElementName))
	    .setText(getResources().getIdentifier(elementIdPrefix + "name"
	    		, "string", getActivity().getPackageName()));
	    
	    ((TextView)elementBigView.findViewById(R.id.tvBigElementAtomicWeight))
	    .setText(getResources().getIdentifier(elementIdPrefix + "atomic_weight"
	    		, "string", getActivity().getPackageName()));
	    
	    
	    String electronsPerShell = getString(getResources().getIdentifier(elementIdPrefix + "eletrons_per_shell"
	    		, "string", getActivity().getPackageName())).replace(" ", "-");
	    
	    ((TextView)this.elementBigView.findViewById(R.id.tvBigElementElectronsPerShell)).setText(electronsPerShell);
	    
	    prevElementView = miniElementView;
		
	} 
	
	 
	private List<Element> getElementList() {
		List<Element> returnList = new ArrayList<Element>();
		
		List<Integer> otherNonMetalList = Arrays.asList(1,6,7,8,15,16,34);
		List<Integer> alkaliMetalList = Arrays.asList(3,11,19,37,55,87);
		List<Integer> alkaliEarthMetalList = Arrays.asList(4,12,20,38,56,88);
		List<Integer> metalloidsList = Arrays.asList(5,14,32,33,51,52,84);
		List<Integer> nobleGaseList = Arrays.asList(2,10,18,36,54,86,118);
		List<Integer> halogensList = Arrays.asList(9,17,35,53,85,117);
		List<Integer> postTransitionMetalsList = Arrays.asList(13,31,49,50,81,82,83,113,114,115,116);
		
		int elementType = -1;
		for(int elementNo = 1 ; elementNo <= ELEMENT_COUNT; elementNo++) {
			
			if(otherNonMetalList.contains(elementNo)) {
				elementType = Element.TYPE_OTHER_NON_METALS;
			} else if (alkaliMetalList.contains(elementNo)) {
				elementType = Element.TYPE_ALKALI_METALS;
			} else if (alkaliEarthMetalList.contains(elementNo)) {
				elementType = Element.TYPE_ALKALINE_EARTH_METALS;
			} else if (metalloidsList.contains(elementNo)) {
				elementType = Element.TYPE_METALLOIDS;
			} else if (nobleGaseList.contains(elementNo)) {
				elementType = Element.TYPE_NOBLE_GASES;
			} else if (halogensList.contains(elementNo)) {
				elementType = Element.TYPE_HALOGENS;
			} else if (postTransitionMetalsList.contains(elementNo)) {
				elementType = Element.TYPE_POST_TRANSITION_METALS;
			} else if (((elementNo >= 21) && (elementNo <= 30)) 
					|| ((elementNo >= 39) && (elementNo <= 48)) 
					|| ((elementNo >= 72) && (elementNo <= 80)) 
					|| ((elementNo >= 104) && (elementNo <= 112))) {
				elementType = Element.TYPE_TRANSITION_METALS;
	        } else if ((elementNo >= 57) && (elementNo <= 71)) {
	        	elementType = Element.TYPE_LANTHANOIDS;
	        } else if ((elementNo >= 89) && (elementNo <= 103)) {
	        	elementType = Element.TYPE_ACTINOIDS;
	        } else {
	        	Log.e(this.getClass().getSimpleName(), "elementNum is not matched with any type");
	        }
			
			Element elem = new Element(elementNo, elementType, "Na"); //change element name param 
			returnList.add(elem);
		}
		return returnList;
	}
	  
	  public int getLegendViewIdByType(int type)
	  {
	    switch (type)
	    {
	    
		    case Element.TYPE_OTHER_NON_METALS: 
		      return R.id.llTypeOtherNonmetals;
		    
		    case Element.TYPE_NOBLE_GASES: 
		      return R.id.llTypeNobleGases;
		    
		    case Element.TYPE_HALOGENS: 
		      return R.id.llTypeHalogens;
		    
		    case Element.TYPE_METALLOIDS: 
		      return R.id.llTypeMetalloids;
		    
		    case Element.TYPE_POST_TRANSITION_METALS: 
		      return R.id.llTypePostTransitionMetals;
		    
		    case Element.TYPE_TRANSITION_METALS: 
		      return R.id.llTypeTransitionMetals;
		    
		    case Element.TYPE_LANTHANOIDS: 
		      return R.id.llTypeLanthanoids;
		    
		    case Element.TYPE_ACTINOIDS: 
		      return R.id.llTypeActinoids;
		    
		    case Element.TYPE_ALKALINE_EARTH_METALS: 
		      return R.id.llTypeAlkalineEarthMetals;
		    
		    case Element.TYPE_ALKALI_METALS: 
		    	return R.id.llTypeAlkalimetals;
		    default: 
			      return -1;
	    }
	  }
	
	 @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 return inflater.inflate(R.layout.periodic_table_fragment, container,false);
		
	}
	
}
