package com.eakash.periodictable.misc;

import com.eakash.periodictable.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MiniElementView extends LinearLayout {

	private Context mContext;
	private Resources mResources;
	private Element mElement;
	private int cellSize;
	private int margin = 2;
	
	
	private TextView tvSymbol;
	private TextView tvAtomicNumber;
	
	private int colorBlack;
	
	public MiniElementView(Context context,Element element,int cellsize) {
		super(context);
		mContext = context;
		mResources = mContext.getResources();
		mElement = element;
		cellSize = cellsize;
		
		colorBlack = mResources.getColor(R.color.pt_black);
		setMainContent();
	}
	
	private void setMainContent() {
		LinearLayout.LayoutParams mainLayoutParams = new LinearLayout.LayoutParams(cellSize, cellSize);
		mainLayoutParams.setMargins(margin, margin, margin, margin);
		setLayoutParams(mainLayoutParams);
		setBackgroundResource(mElement.getTypeDrawableResId(false));
		setOrientation(LinearLayout.VERTICAL);
		
		addElementView();
		addElementContent();
	}
	
	private void addElementView() {
		setClickable(true);
		
		tvAtomicNumber = new TextView(mContext);
		tvAtomicNumber.setIncludeFontPadding(false);
		tvAtomicNumber.setTextSize(7);
		tvAtomicNumber.setTextColor(colorBlack);
		tvAtomicNumber.setPadding(0, 1, 3, 0);
		
		LayoutParams tvAtomicNumberParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		tvAtomicNumberParams.gravity = Gravity.RIGHT;
		tvAtomicNumberParams.weight = 1.0F;
		addView(tvAtomicNumber, tvAtomicNumberParams);
		
		tvSymbol = new TextView(mContext);
		tvSymbol.setIncludeFontPadding(false);
		tvSymbol.setTextSize(12);
		tvSymbol.setTextColor(colorBlack);
		tvSymbol.setTypeface(null,Typeface.BOLD);
		tvSymbol.setPadding(1, 0, 0, 1);
		
		LayoutParams tvSymbolParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		tvSymbolParams.gravity = Gravity.CENTER;
		addView(tvSymbol, tvSymbolParams);
		
	}
	
	private void addElementContent() {
		String idPrefix = "element_" + mElement.AtomicNumber + "_";
		if(tvSymbol != null) {
			tvSymbol.setText(mResources.getIdentifier(idPrefix+"symbol", "string", mContext.getPackageName()));
		}
		if(tvAtomicNumber != null) {
			tvAtomicNumber.setText(mResources.getIdentifier(idPrefix+"atomic_number", "string", mContext.getPackageName()));
		}
	}
	public void setActivated(boolean isHovered){
	    setBackgroundResource(mElement.getTypeDrawableResId(isHovered));
	}
	public Element getElement(){
	    return mElement;
	  }

}
