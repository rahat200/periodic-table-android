package com.eakash.periodictable.misc;

import com.eakash.periodictable.R;

public class Element {
	
	public static final int TYPE_OTHER_NON_METALS = 0;
	public static final int TYPE_NOBLE_GASES = 1;
	public static final int TYPE_HALOGENS = 2;
	public static final int TYPE_METALLOIDS = 3;
	public static final int TYPE_POST_TRANSITION_METALS = 4;
	public static final int TYPE_TRANSITION_METALS = 5;
	public static final int TYPE_LANTHANOIDS = 6;
	public static final int TYPE_ACTINOIDS = 7;
	public static final int TYPE_ALKALINE_EARTH_METALS = 8;
	public static final int TYPE_ALKALI_METALS = 9;
	
	public int AtomicNumber = -1;
	public String Name = null;
	public int Type = -1;
	
	public Element(int atomic_num, int type, String name) {
	    this.AtomicNumber = atomic_num;
	    this.Type = type;
	    this.Name = name;
	}
	
	public String getName() {
	    return this.Name;
	}
	
	public int getTypeColorResId() {
	    
		switch (this.Type) {
	    	case TYPE_OTHER_NON_METALS: 
		      return R.color.type_other_nonmetals;
		    case TYPE_NOBLE_GASES: 
		      return R.color.type_noble_gases;
		    case TYPE_HALOGENS: 
		      return R.color.type_halogens;
		    case TYPE_METALLOIDS: 
		      return R.color.type_metalloids;
		    case TYPE_POST_TRANSITION_METALS: 
		      return R.color.type_post_transition_metals;
		    case TYPE_TRANSITION_METALS: 
		      return R.color.type_transition_metals;
		    case TYPE_LANTHANOIDS: 
		      return R.color.type_lanthanoids;
		    case TYPE_ACTINOIDS: 
		      return R.color.type_actinoids;
		    case TYPE_ALKALINE_EARTH_METALS: 
		      return R.color.type_alkaline_earth_metals;
		    case TYPE_ALKALI_METALS: 
		    	return R.color.type_alkali_metals;
		    default: 
			    return -1;
	    }
	}
	
	
	
	  public int getTypeDrawableResId(boolean isHovered) {
	    switch (this.Type)
	    {
		    case TYPE_OTHER_NON_METALS: 
			      if (isHovered) {
			        return R.drawable.border_type_other_nonmetals_on;
			      }
			      return R.drawable.border_type_other_nonmetals;
		    case TYPE_NOBLE_GASES: 
			      if (isHovered) {
			        return R.drawable.border_type_noble_gases_on;
			      }
			      return R.drawable.border_type_noble_gases;
		    case TYPE_HALOGENS: 
			      if (isHovered) {
			        return R.drawable.border_type_halogens_on;
			      }
			      return R.drawable.border_type_halogens;
		    case TYPE_METALLOIDS: 
			      if (isHovered) {
			        return R.drawable.border_type_metalloids_on;
			      }
			      return R.drawable.border_type_metalloids;
		    case TYPE_POST_TRANSITION_METALS: 
			      if (isHovered) {
			        return R.drawable.border_type_post_transition_metals_on;
			      }
			      return R.drawable.border_type_post_transition_metals;
		    case TYPE_TRANSITION_METALS: 
			      if (isHovered) {
			        return R.drawable.border_type_transition_metals_on;
			      }
			      return R.drawable.border_type_transition_metals;
		    case TYPE_LANTHANOIDS: 
			      if (isHovered) {
			        return R.drawable.border_type_lanthanoids_on;
			      }
			      return R.drawable.border_type_lanthanoids;
		    case TYPE_ACTINOIDS: 
			      if (isHovered) {
			        return R.drawable.border_type_actinoids_on;
			      }
			      return R.drawable.border_type_actinoids;
		    case TYPE_ALKALINE_EARTH_METALS: 
			      if (isHovered) {
			        return R.drawable.border_type_alkaline_earth_metals_on;
			      }
			      return R.drawable.border_type_alkaline_earth_metals;
		    case TYPE_ALKALI_METALS: 
				   if (isHovered) {
				     return R.drawable.border_type_alkali_metals_on;
				   }
				   return R.drawable.border_type_alkali_metals;
		    default: 
			      return -1;
	    }
	  }
	
	  public int getTypeText() {
	    switch (this.Type)
	    {
		    case TYPE_OTHER_NON_METALS: 
		      return R.string.type_element_othernonmetals;
		    case TYPE_NOBLE_GASES: 
		      return R.string.type_element_noble_gases;
		    case TYPE_HALOGENS: 
		      return R.string.type_element_halogens;
		    case TYPE_METALLOIDS: 
		      return R.string.type_element_metalloids;
		    case TYPE_POST_TRANSITION_METALS: 
		      return R.string.type_element_post_transition_metals;
		    case TYPE_TRANSITION_METALS: 
		      return R.string.type_element_transition_metals;
		    case TYPE_LANTHANOIDS: 
		      return R.string.type_element_lanthanoids;
		    case TYPE_ACTINOIDS: 
		      return R.string.type_element_actinoids;
		    case TYPE_ALKALINE_EARTH_METALS: 
		      return R.string.type_element_alkaline_earth_metals;
		    case TYPE_ALKALI_METALS: 
		    	return R.string.type_element_alkali_metals;
		    default: 
			      return -1;
		    }
	  }
}
